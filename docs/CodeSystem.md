# CodeSystem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**case_sensitive** | **bool** |  | [optional] 
**copyright** | **str** |  | [optional] 
**created_time** | **datetime** |  | [optional] 
**date** | **datetime** |  | [optional] 
**id** | **str** |  | [optional] 
**inline** | **bool** |  | [optional] 
**name** | **str** |  | [optional] 
**owner_id** | **str** |  | [optional] 
**publisher** | [**Organisation**](Organisation.md) |  | [optional] 
**short_name** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**system** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**updated_by** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


