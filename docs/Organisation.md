# Organisation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_systems** | [**list[CodeSystem]**](CodeSystem.md) |  | [optional] 
**contact_email** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


