# coding: utf-8

"""
    Termlex API

    Termlex is a RESTful, service oriented terminology server and SNOMED CT API

    OpenAPI spec version: 3.2.0
    Contact: info@noesisinformatica.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from pprint import pformat
from six import iteritems
import re


class MetaData(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """
    def __init__(self, accepted_date=None, accrual_methods=None, accrual_periodicities=None, accrual_policies=None, audience_education_levels=None, audience_mediators=None, available_date=None, contributors=None, copyrighted_date=None, created_date=None, creators=None, description=None, format=None, id=None, identifier=None, instructional_methods=None, issued_date=None, language=None, last_updated_date=None, licenses=None, modified_dates=None, provenances=None, publishers=None, replaced_date=None, retired_date=None, rights_holders=None, sources=None, subject=None, submitted_date=None, temporal_coverage=None, title=None, type=None, valid_date=None):
        """
        MetaData - a model defined in Swagger

        :param dict swaggerTypes: The key is attribute name
                                  and the value is attribute type.
        :param dict attributeMap: The key is attribute name
                                  and the value is json key in definition.
        """
        self.swagger_types = {
            'accepted_date': 'datetime',
            'accrual_methods': 'list[str]',
            'accrual_periodicities': 'list[str]',
            'accrual_policies': 'list[str]',
            'audience_education_levels': 'list[str]',
            'audience_mediators': 'list[str]',
            'available_date': 'datetime',
            'contributors': 'list[Person]',
            'copyrighted_date': 'datetime',
            'created_date': 'datetime',
            'creators': 'list[Person]',
            'description': 'str',
            'format': 'str',
            'id': 'str',
            'identifier': 'URI',
            'instructional_methods': 'list[str]',
            'issued_date': 'datetime',
            'language': 'str',
            'last_updated_date': 'datetime',
            'licenses': 'list[URI]',
            'modified_dates': 'list[datetime]',
            'provenances': 'list[str]',
            'publishers': 'list[Person]',
            'replaced_date': 'datetime',
            'retired_date': 'datetime',
            'rights_holders': 'list[Person]',
            'sources': 'list[URI]',
            'subject': 'str',
            'submitted_date': 'datetime',
            'temporal_coverage': 'CollectionOfCalendar',
            'title': 'str',
            'type': 'str',
            'valid_date': 'datetime'
        }

        self.attribute_map = {
            'accepted_date': 'acceptedDate',
            'accrual_methods': 'accrualMethods',
            'accrual_periodicities': 'accrualPeriodicities',
            'accrual_policies': 'accrualPolicies',
            'audience_education_levels': 'audienceEducationLevels',
            'audience_mediators': 'audienceMediators',
            'available_date': 'availableDate',
            'contributors': 'contributors',
            'copyrighted_date': 'copyrightedDate',
            'created_date': 'createdDate',
            'creators': 'creators',
            'description': 'description',
            'format': 'format',
            'id': 'id',
            'identifier': 'identifier',
            'instructional_methods': 'instructionalMethods',
            'issued_date': 'issuedDate',
            'language': 'language',
            'last_updated_date': 'lastUpdatedDate',
            'licenses': 'licenses',
            'modified_dates': 'modifiedDates',
            'provenances': 'provenances',
            'publishers': 'publishers',
            'replaced_date': 'replacedDate',
            'retired_date': 'retiredDate',
            'rights_holders': 'rightsHolders',
            'sources': 'sources',
            'subject': 'subject',
            'submitted_date': 'submittedDate',
            'temporal_coverage': 'temporalCoverage',
            'title': 'title',
            'type': 'type',
            'valid_date': 'validDate'
        }

        self._accepted_date = accepted_date
        self._accrual_methods = accrual_methods
        self._accrual_periodicities = accrual_periodicities
        self._accrual_policies = accrual_policies
        self._audience_education_levels = audience_education_levels
        self._audience_mediators = audience_mediators
        self._available_date = available_date
        self._contributors = contributors
        self._copyrighted_date = copyrighted_date
        self._created_date = created_date
        self._creators = creators
        self._description = description
        self._format = format
        self._id = id
        self._identifier = identifier
        self._instructional_methods = instructional_methods
        self._issued_date = issued_date
        self._language = language
        self._last_updated_date = last_updated_date
        self._licenses = licenses
        self._modified_dates = modified_dates
        self._provenances = provenances
        self._publishers = publishers
        self._replaced_date = replaced_date
        self._retired_date = retired_date
        self._rights_holders = rights_holders
        self._sources = sources
        self._subject = subject
        self._submitted_date = submitted_date
        self._temporal_coverage = temporal_coverage
        self._title = title
        self._type = type
        self._valid_date = valid_date

    @property
    def accepted_date(self):
        """
        Gets the accepted_date of this MetaData.


        :return: The accepted_date of this MetaData.
        :rtype: datetime
        """
        return self._accepted_date

    @accepted_date.setter
    def accepted_date(self, accepted_date):
        """
        Sets the accepted_date of this MetaData.


        :param accepted_date: The accepted_date of this MetaData.
        :type: datetime
        """

        self._accepted_date = accepted_date

    @property
    def accrual_methods(self):
        """
        Gets the accrual_methods of this MetaData.


        :return: The accrual_methods of this MetaData.
        :rtype: list[str]
        """
        return self._accrual_methods

    @accrual_methods.setter
    def accrual_methods(self, accrual_methods):
        """
        Sets the accrual_methods of this MetaData.


        :param accrual_methods: The accrual_methods of this MetaData.
        :type: list[str]
        """
        allowed_values = ["CONSULTATION", "MANDATED", "REQUESTED"]
        if accrual_methods not in allowed_values:
            raise ValueError(
                "Invalid value for `accrual_methods` ({0}), must be one of {1}"
                .format(accrual_methods, allowed_values)
            )

        self._accrual_methods = accrual_methods

    @property
    def accrual_periodicities(self):
        """
        Gets the accrual_periodicities of this MetaData.


        :return: The accrual_periodicities of this MetaData.
        :rtype: list[str]
        """
        return self._accrual_periodicities

    @accrual_periodicities.setter
    def accrual_periodicities(self, accrual_periodicities):
        """
        Sets the accrual_periodicities of this MetaData.


        :param accrual_periodicities: The accrual_periodicities of this MetaData.
        :type: list[str]
        """
        allowed_values = ["MONTHLY", "ANNUAL", "BI_ANNUAL", "WEEKLY", "FORTNIGHTLY", "IRREGULAR"]
        if accrual_periodicities not in allowed_values:
            raise ValueError(
                "Invalid value for `accrual_periodicities` ({0}), must be one of {1}"
                .format(accrual_periodicities, allowed_values)
            )

        self._accrual_periodicities = accrual_periodicities

    @property
    def accrual_policies(self):
        """
        Gets the accrual_policies of this MetaData.


        :return: The accrual_policies of this MetaData.
        :rtype: list[str]
        """
        return self._accrual_policies

    @accrual_policies.setter
    def accrual_policies(self, accrual_policies):
        """
        Sets the accrual_policies of this MetaData.


        :param accrual_policies: The accrual_policies of this MetaData.
        :type: list[str]
        """
        allowed_values = ["OPEN", "CLOSED"]
        if accrual_policies not in allowed_values:
            raise ValueError(
                "Invalid value for `accrual_policies` ({0}), must be one of {1}"
                .format(accrual_policies, allowed_values)
            )

        self._accrual_policies = accrual_policies

    @property
    def audience_education_levels(self):
        """
        Gets the audience_education_levels of this MetaData.


        :return: The audience_education_levels of this MetaData.
        :rtype: list[str]
        """
        return self._audience_education_levels

    @audience_education_levels.setter
    def audience_education_levels(self, audience_education_levels):
        """
        Sets the audience_education_levels of this MetaData.


        :param audience_education_levels: The audience_education_levels of this MetaData.
        :type: list[str]
        """

        self._audience_education_levels = audience_education_levels

    @property
    def audience_mediators(self):
        """
        Gets the audience_mediators of this MetaData.


        :return: The audience_mediators of this MetaData.
        :rtype: list[str]
        """
        return self._audience_mediators

    @audience_mediators.setter
    def audience_mediators(self, audience_mediators):
        """
        Sets the audience_mediators of this MetaData.


        :param audience_mediators: The audience_mediators of this MetaData.
        :type: list[str]
        """

        self._audience_mediators = audience_mediators

    @property
    def available_date(self):
        """
        Gets the available_date of this MetaData.


        :return: The available_date of this MetaData.
        :rtype: datetime
        """
        return self._available_date

    @available_date.setter
    def available_date(self, available_date):
        """
        Sets the available_date of this MetaData.


        :param available_date: The available_date of this MetaData.
        :type: datetime
        """

        self._available_date = available_date

    @property
    def contributors(self):
        """
        Gets the contributors of this MetaData.


        :return: The contributors of this MetaData.
        :rtype: list[Person]
        """
        return self._contributors

    @contributors.setter
    def contributors(self, contributors):
        """
        Sets the contributors of this MetaData.


        :param contributors: The contributors of this MetaData.
        :type: list[Person]
        """

        self._contributors = contributors

    @property
    def copyrighted_date(self):
        """
        Gets the copyrighted_date of this MetaData.


        :return: The copyrighted_date of this MetaData.
        :rtype: datetime
        """
        return self._copyrighted_date

    @copyrighted_date.setter
    def copyrighted_date(self, copyrighted_date):
        """
        Sets the copyrighted_date of this MetaData.


        :param copyrighted_date: The copyrighted_date of this MetaData.
        :type: datetime
        """

        self._copyrighted_date = copyrighted_date

    @property
    def created_date(self):
        """
        Gets the created_date of this MetaData.


        :return: The created_date of this MetaData.
        :rtype: datetime
        """
        return self._created_date

    @created_date.setter
    def created_date(self, created_date):
        """
        Sets the created_date of this MetaData.


        :param created_date: The created_date of this MetaData.
        :type: datetime
        """

        self._created_date = created_date

    @property
    def creators(self):
        """
        Gets the creators of this MetaData.


        :return: The creators of this MetaData.
        :rtype: list[Person]
        """
        return self._creators

    @creators.setter
    def creators(self, creators):
        """
        Sets the creators of this MetaData.


        :param creators: The creators of this MetaData.
        :type: list[Person]
        """

        self._creators = creators

    @property
    def description(self):
        """
        Gets the description of this MetaData.


        :return: The description of this MetaData.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """
        Sets the description of this MetaData.


        :param description: The description of this MetaData.
        :type: str
        """

        self._description = description

    @property
    def format(self):
        """
        Gets the format of this MetaData.


        :return: The format of this MetaData.
        :rtype: str
        """
        return self._format

    @format.setter
    def format(self, format):
        """
        Sets the format of this MetaData.


        :param format: The format of this MetaData.
        :type: str
        """

        self._format = format

    @property
    def id(self):
        """
        Gets the id of this MetaData.


        :return: The id of this MetaData.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this MetaData.


        :param id: The id of this MetaData.
        :type: str
        """

        self._id = id

    @property
    def identifier(self):
        """
        Gets the identifier of this MetaData.


        :return: The identifier of this MetaData.
        :rtype: URI
        """
        return self._identifier

    @identifier.setter
    def identifier(self, identifier):
        """
        Sets the identifier of this MetaData.


        :param identifier: The identifier of this MetaData.
        :type: URI
        """

        self._identifier = identifier

    @property
    def instructional_methods(self):
        """
        Gets the instructional_methods of this MetaData.


        :return: The instructional_methods of this MetaData.
        :rtype: list[str]
        """
        return self._instructional_methods

    @instructional_methods.setter
    def instructional_methods(self, instructional_methods):
        """
        Sets the instructional_methods of this MetaData.


        :param instructional_methods: The instructional_methods of this MetaData.
        :type: list[str]
        """

        self._instructional_methods = instructional_methods

    @property
    def issued_date(self):
        """
        Gets the issued_date of this MetaData.


        :return: The issued_date of this MetaData.
        :rtype: datetime
        """
        return self._issued_date

    @issued_date.setter
    def issued_date(self, issued_date):
        """
        Sets the issued_date of this MetaData.


        :param issued_date: The issued_date of this MetaData.
        :type: datetime
        """

        self._issued_date = issued_date

    @property
    def language(self):
        """
        Gets the language of this MetaData.


        :return: The language of this MetaData.
        :rtype: str
        """
        return self._language

    @language.setter
    def language(self, language):
        """
        Sets the language of this MetaData.


        :param language: The language of this MetaData.
        :type: str
        """

        self._language = language

    @property
    def last_updated_date(self):
        """
        Gets the last_updated_date of this MetaData.


        :return: The last_updated_date of this MetaData.
        :rtype: datetime
        """
        return self._last_updated_date

    @last_updated_date.setter
    def last_updated_date(self, last_updated_date):
        """
        Sets the last_updated_date of this MetaData.


        :param last_updated_date: The last_updated_date of this MetaData.
        :type: datetime
        """

        self._last_updated_date = last_updated_date

    @property
    def licenses(self):
        """
        Gets the licenses of this MetaData.


        :return: The licenses of this MetaData.
        :rtype: list[URI]
        """
        return self._licenses

    @licenses.setter
    def licenses(self, licenses):
        """
        Sets the licenses of this MetaData.


        :param licenses: The licenses of this MetaData.
        :type: list[URI]
        """

        self._licenses = licenses

    @property
    def modified_dates(self):
        """
        Gets the modified_dates of this MetaData.


        :return: The modified_dates of this MetaData.
        :rtype: list[datetime]
        """
        return self._modified_dates

    @modified_dates.setter
    def modified_dates(self, modified_dates):
        """
        Sets the modified_dates of this MetaData.


        :param modified_dates: The modified_dates of this MetaData.
        :type: list[datetime]
        """

        self._modified_dates = modified_dates

    @property
    def provenances(self):
        """
        Gets the provenances of this MetaData.


        :return: The provenances of this MetaData.
        :rtype: list[str]
        """
        return self._provenances

    @provenances.setter
    def provenances(self, provenances):
        """
        Sets the provenances of this MetaData.


        :param provenances: The provenances of this MetaData.
        :type: list[str]
        """

        self._provenances = provenances

    @property
    def publishers(self):
        """
        Gets the publishers of this MetaData.


        :return: The publishers of this MetaData.
        :rtype: list[Person]
        """
        return self._publishers

    @publishers.setter
    def publishers(self, publishers):
        """
        Sets the publishers of this MetaData.


        :param publishers: The publishers of this MetaData.
        :type: list[Person]
        """

        self._publishers = publishers

    @property
    def replaced_date(self):
        """
        Gets the replaced_date of this MetaData.


        :return: The replaced_date of this MetaData.
        :rtype: datetime
        """
        return self._replaced_date

    @replaced_date.setter
    def replaced_date(self, replaced_date):
        """
        Sets the replaced_date of this MetaData.


        :param replaced_date: The replaced_date of this MetaData.
        :type: datetime
        """

        self._replaced_date = replaced_date

    @property
    def retired_date(self):
        """
        Gets the retired_date of this MetaData.


        :return: The retired_date of this MetaData.
        :rtype: datetime
        """
        return self._retired_date

    @retired_date.setter
    def retired_date(self, retired_date):
        """
        Sets the retired_date of this MetaData.


        :param retired_date: The retired_date of this MetaData.
        :type: datetime
        """

        self._retired_date = retired_date

    @property
    def rights_holders(self):
        """
        Gets the rights_holders of this MetaData.


        :return: The rights_holders of this MetaData.
        :rtype: list[Person]
        """
        return self._rights_holders

    @rights_holders.setter
    def rights_holders(self, rights_holders):
        """
        Sets the rights_holders of this MetaData.


        :param rights_holders: The rights_holders of this MetaData.
        :type: list[Person]
        """

        self._rights_holders = rights_holders

    @property
    def sources(self):
        """
        Gets the sources of this MetaData.


        :return: The sources of this MetaData.
        :rtype: list[URI]
        """
        return self._sources

    @sources.setter
    def sources(self, sources):
        """
        Sets the sources of this MetaData.


        :param sources: The sources of this MetaData.
        :type: list[URI]
        """

        self._sources = sources

    @property
    def subject(self):
        """
        Gets the subject of this MetaData.


        :return: The subject of this MetaData.
        :rtype: str
        """
        return self._subject

    @subject.setter
    def subject(self, subject):
        """
        Sets the subject of this MetaData.


        :param subject: The subject of this MetaData.
        :type: str
        """

        self._subject = subject

    @property
    def submitted_date(self):
        """
        Gets the submitted_date of this MetaData.


        :return: The submitted_date of this MetaData.
        :rtype: datetime
        """
        return self._submitted_date

    @submitted_date.setter
    def submitted_date(self, submitted_date):
        """
        Sets the submitted_date of this MetaData.


        :param submitted_date: The submitted_date of this MetaData.
        :type: datetime
        """

        self._submitted_date = submitted_date

    @property
    def temporal_coverage(self):
        """
        Gets the temporal_coverage of this MetaData.


        :return: The temporal_coverage of this MetaData.
        :rtype: CollectionOfCalendar
        """
        return self._temporal_coverage

    @temporal_coverage.setter
    def temporal_coverage(self, temporal_coverage):
        """
        Sets the temporal_coverage of this MetaData.


        :param temporal_coverage: The temporal_coverage of this MetaData.
        :type: CollectionOfCalendar
        """

        self._temporal_coverage = temporal_coverage

    @property
    def title(self):
        """
        Gets the title of this MetaData.


        :return: The title of this MetaData.
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """
        Sets the title of this MetaData.


        :param title: The title of this MetaData.
        :type: str
        """

        self._title = title

    @property
    def type(self):
        """
        Gets the type of this MetaData.


        :return: The type of this MetaData.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this MetaData.


        :param type: The type of this MetaData.
        :type: str
        """
        allowed_values = ["COLLECTION", "DATASET", "EVENT", "IMAGE", "INTERACTIVE_RESOURCE", "MOVING_IMAGE", "PHYSICAL_OBJECT", "SERVICE", "SOFTWARE", "SOUND", "STILL_IMAGE", "TEXT"]
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"
                .format(type, allowed_values)
            )

        self._type = type

    @property
    def valid_date(self):
        """
        Gets the valid_date of this MetaData.


        :return: The valid_date of this MetaData.
        :rtype: datetime
        """
        return self._valid_date

    @valid_date.setter
    def valid_date(self, valid_date):
        """
        Sets the valid_date of this MetaData.


        :param valid_date: The valid_date of this MetaData.
        :type: datetime
        """

        self._valid_date = valid_date

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
