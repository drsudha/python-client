# swagger_client.ExpressionresourceApi

All URIs are relative to *https://localhost/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_compositional_grammar_form_using_get**](ExpressionresourceApi.md#get_compositional_grammar_form_using_get) | **GET** /api/expressions/cgf | get the compositional grammar form of expression
[**get_long_normal_form_using_get**](ExpressionresourceApi.md#get_long_normal_form_using_get) | **GET** /api/expressions/lnf | get the long normal form of expression
[**get_short_normal_form_using_get**](ExpressionresourceApi.md#get_short_normal_form_using_get) | **GET** /api/expressions/snf | get the long normal form of expression
[**get_subsumption_relationship_using_get1**](ExpressionresourceApi.md#get_subsumption_relationship_using_get1) | **GET** /api/expressions/compare | get the subsumption relationship of comparing two expressions


# **get_compositional_grammar_form_using_get**
> str get_compositional_grammar_form_using_get(expression=expression)

get the compositional grammar form of expression

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExpressionresourceApi()
expression = 'expression_example' # str | The expression to be rendered (optional)

try: 
    # get the compositional grammar form of expression
    api_response = api_instance.get_compositional_grammar_form_using_get(expression=expression)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ExpressionresourceApi->get_compositional_grammar_form_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression** | **str**| The expression to be rendered | [optional] 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_long_normal_form_using_get**
> NormalFormExpression get_long_normal_form_using_get(expression=expression, version=version)

get the long normal form of expression

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExpressionresourceApi()
expression = 'expression_example' # str | The expression to be rendered (optional)
version = 'version_example' # str | The version of the terminology product as date string (optional)

try: 
    # get the long normal form of expression
    api_response = api_instance.get_long_normal_form_using_get(expression=expression, version=version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ExpressionresourceApi->get_long_normal_form_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression** | **str**| The expression to be rendered | [optional] 
 **version** | **str**| The version of the terminology product as date string | [optional] 

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_short_normal_form_using_get**
> NormalFormExpression get_short_normal_form_using_get(expression=expression, version=version)

get the long normal form of expression

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExpressionresourceApi()
expression = 'expression_example' # str | The expression to be rendered (optional)
version = 'version_example' # str | The version of the terminology product as date string (optional)

try: 
    # get the long normal form of expression
    api_response = api_instance.get_short_normal_form_using_get(expression=expression, version=version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ExpressionresourceApi->get_short_normal_form_using_get: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression** | **str**| The expression to be rendered | [optional] 
 **version** | **str**| The version of the terminology product as date string | [optional] 

### Return type

[**NormalFormExpression**](NormalFormExpression.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_subsumption_relationship_using_get1**
> str get_subsumption_relationship_using_get1(expression1=expression1, expression2=expression2, version=version)

get the subsumption relationship of comparing two expressions

When a product and version are not set, then a default Terminology Product and version are assumed. This usually defaults to the latest SNOMED CT release loaded into Termlex.

### Example 
```python
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.ExpressionresourceApi()
expression1 = 'expression1_example' # str | The left hand side expression to be compared (optional)
expression2 = 'expression2_example' # str | The right hand side expression to be compared (optional)
version = 'version_example' # str | The version of the terminology product as date string (optional)

try: 
    # get the subsumption relationship of comparing two expressions
    api_response = api_instance.get_subsumption_relationship_using_get1(expression1=expression1, expression2=expression2, version=version)
    pprint(api_response)
except ApiException as e:
    print "Exception when calling ExpressionresourceApi->get_subsumption_relationship_using_get1: %s\n" % e
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expression1** | **str**| The left hand side expression to be compared | [optional] 
 **expression2** | **str**| The right hand side expression to be compared | [optional] 
 **version** | **str**| The version of the terminology product as date string | [optional] 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

