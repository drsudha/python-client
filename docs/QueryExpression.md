# QueryExpression

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contained_expressions** | [**list[QueryExpression]**](QueryExpression.md) |  | [optional] 
**human_readable_statement** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**meta_data** | [**MetaData**](MetaData.md) |  | [optional] 
**run_time_status** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


