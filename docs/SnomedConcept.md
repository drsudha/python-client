# SnomedConcept

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**active_concept** | **bool** |  | [optional] 
**child_ids** | [**CollectionOfstring**](CollectionOfstring.md) |  | [optional] 
**children** | [**CollectionOfSnomedConcept**](CollectionOfSnomedConcept.md) |  | [optional] 
**code_system** | [**CodeSystem**](CodeSystem.md) |  | [optional] 
**created_time** | **datetime** |  | [optional] 
**ctv3_id** | **str** |  | [optional] 
**defining_relationships** | [**CollectionOfSnomedRelationship**](CollectionOfSnomedRelationship.md) |  | [optional] 
**descriptions** | [**CollectionOfSnomedDescription**](CollectionOfSnomedDescription.md) |  | [optional] 
**effective_time** | [**Calendar**](Calendar.md) |  | [optional] 
**flavour** | **str** |  | [optional] 
**fully_specified_name** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**inactivated_time** | **datetime** |  | [optional] 
**last_updated_time** | **datetime** |  | [optional] 
**module_id** | **int** |  | [optional] 
**non_sub_sumption_defining_relationships** | [**CollectionOfSnomedRelationship**](CollectionOfSnomedRelationship.md) |  | [optional] 
**parent_ids** | [**CollectionOfstring**](CollectionOfstring.md) |  | [optional] 
**parents** | [**CollectionOfSnomedConcept**](CollectionOfSnomedConcept.md) |  | [optional] 
**preferred_term** | **str** |  | [optional] 
**primitive** | **bool** |  | [optional] 
**refining_relationships** | [**CollectionOfSnomedRelationship**](CollectionOfSnomedRelationship.md) |  | [optional] 
**relationships** | [**CollectionOfSnomedRelationship**](CollectionOfSnomedRelationship.md) |  | [optional] 
**role_groups** | [**CollectionOfSnomedRoleGroup**](CollectionOfSnomedRoleGroup.md) |  | [optional] 
**snomed_id** | **str** |  | [optional] 
**source** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**synonyms** | [**CollectionOfstring**](CollectionOfstring.md) |  | [optional] 
**textual_definitions** | [**CollectionOfSnomedDescription**](CollectionOfSnomedDescription.md) |  | [optional] 
**type** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


